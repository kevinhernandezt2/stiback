/**
 * 
 */
package com.sti.prueba.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sti.prueba.models.dao.EstatusDAO;
import com.sti.prueba.models.entity.Estatus;
import com.sti.prueba.service.IEstatusService;

/**
 * @author khernandez
 *
 */
@Service
public class EstatusServiceImpl implements IEstatusService {
	
	@Autowired
	private EstatusDAO estatusDAO;

	@Override
	@Transactional(readOnly = true)
	public List<Estatus> listarEstatus() {
		return estatusDAO.findAll();
	}

	

}
