/**
 * 
 */
package com.sti.prueba.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sti.prueba.dto.ActividadDTO;
import com.sti.prueba.dto.ConsultarActividadesDTO;
import com.sti.prueba.models.dao.ActividadDAO;
import com.sti.prueba.models.dao.EmpleadoDAO;
import com.sti.prueba.models.dao.EstatusDAO;
import com.sti.prueba.models.entity.Actividad;
import com.sti.prueba.models.entity.Empleado;
import com.sti.prueba.models.entity.Estatus;
import com.sti.prueba.service.IActividadService;
import com.sti.prueba.utils.ConvertidorFecha;

/**
 * @author khernandez
 *
 */
@Service
public class ActividadServiceImpl implements IActividadService {
	
	@Autowired
	private ActividadDAO actividadDAO;
	
	@Autowired
	private EmpleadoDAO empleadoDAO;
	
	@Autowired
	private EstatusDAO estatusDAO;
	
	@Override
	@Transactional(readOnly = true)
	public List<ConsultarActividadesDTO> listarActividades() {
		List<Actividad> actividades = actividadDAO.findAll();
		List<ConsultarActividadesDTO> actividadesDto = new ArrayList<>();
		
		long miliseconds = System.currentTimeMillis();
        Date date = new Date(miliseconds);
        
		actividades.forEach(act -> {
			ConsultarActividadesDTO actividadDTO = new ConsultarActividadesDTO();
			actividadDTO.setId(act.getId());
			actividadDTO.setNombreActividad(act.getNombreActividad());
			actividadDTO.setEstatus(act.getEstatus());
			actividadDTO.setFechaHoraEjecucion(act.getFechaHoraEjecucion().toString());
			
			if((act.getEstatus().getId() == 2) && (act.getFechaHoraEjecucion().getTime() < date.getTime())) {
				int milisecondsByDay = 86400000;
				int dias = (int) ((date.getTime() - act.getFechaHoraEjecucion().getTime()) / milisecondsByDay);
				actividadDTO.setDiasRetraso(dias);
			}else if(act.getEstatus().getId() == 1) {
				actividadDTO.setDiasRetraso(0);
			}else {
				actividadDTO.setDiasRetraso(0);
			}
			
			actividadDTO.setEmpleado(act.getEmpleado());
			
			actividadesDto.add(actividadDTO);
		});
		
		return actividadesDto;
	}

	@Override
	@Transactional
	public Actividad crearActividad(ActividadDTO actividadDto) {
		
		Optional<Estatus> idEstatus = estatusDAO.findById(actividadDto.getEstatus());
		Optional<Empleado> idEmpleado = empleadoDAO.findById(actividadDto.getEmpleado());
		
		Actividad actividad = new Actividad();
		
		actividad.setNombreActividad(actividadDto.getNombreActividad());
		actividad.setEstatus(idEstatus.isPresent() ? idEstatus.get() : null);
		actividad.setFechaHoraEjecucion(ConvertidorFecha.formateoFechaStringTimestamp(actividadDto.getFechaHoraEjecucion()));
		actividad.setEmpleado(idEmpleado.isPresent() ? idEmpleado.get() : null);
		
		return actividadDAO.save(actividad);
	}

	@Override
	@Transactional
	public Actividad editarActividad(ActividadDTO actividadDto, Long id) {
		Actividad actividadActual = actividadDAO.findById(id).orElse(null);
		
		Estatus idEstatus = estatusDAO.findById(actividadDto.getEstatus()).orElse(null);
		Empleado idEmpleado = empleadoDAO.findById(actividadDto.getEmpleado()).orElse(null);
		
		actividadActual.setNombreActividad(actividadDto.getNombreActividad());
		actividadActual.setEstatus(idEstatus);
		actividadActual.setFechaHoraEjecucion(ConvertidorFecha.formateoFechaStringTimestamp(actividadDto.getFechaHoraEjecucion()));
		actividadActual.setEmpleado(idEmpleado);
		
		return actividadDAO.save(actividadActual);
	}

	@Override
	@Transactional
	public void eliminarActividad(Long id) {
		actividadDAO.deleteById(id);
	}

}
