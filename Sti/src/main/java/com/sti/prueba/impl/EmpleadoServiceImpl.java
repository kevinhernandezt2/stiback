/**
 * 
 */
package com.sti.prueba.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sti.prueba.models.dao.EmpleadoDAO;
import com.sti.prueba.models.entity.Empleado;
import com.sti.prueba.service.IEmpleadoService;

/**
 * @author khernandez
 *
 */
@Service
public class EmpleadoServiceImpl implements IEmpleadoService {
	
	@Autowired
	private EmpleadoDAO empleadoDAO;

	@Override
	@Transactional(readOnly = true)
	public List<Empleado> listarEmpleados() {
		return empleadoDAO.findAll();
	}

}
