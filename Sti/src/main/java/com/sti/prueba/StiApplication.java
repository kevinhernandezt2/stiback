package com.sti.prueba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StiApplication {

	public static void main(String[] args) {
		SpringApplication.run(StiApplication.class, args);
	}

}
