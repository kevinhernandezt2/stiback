/**
 * 
 */
package com.sti.prueba.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.sti.prueba.models.entity.Actividad;

/**
 * @author khernandez
 *
 */
@Repository
public interface ActividadDAO extends JpaRepository<Actividad, Long>, JpaSpecificationExecutor<Actividad> {

}
