/**
 * 
 */
package com.sti.prueba.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author khernandez
 *
 */
@Entity
@Table(name="actividad")
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Builder
public class Actividad implements Serializable {
	
private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_actividad")
	private long id;
	
	@Column(name = "nombre_actividad", nullable = false)
	private String nombreActividad;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "estatus")
	private Estatus estatus;
	
	@Column(name = "fecha_hora_eje", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaHoraEjecucion;
	
	@ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "empleado")
	private Empleado empleado;

}
