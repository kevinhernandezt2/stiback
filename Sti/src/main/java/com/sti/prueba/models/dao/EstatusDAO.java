/**
 * 
 */
package com.sti.prueba.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.sti.prueba.models.entity.Estatus;

/**
 * @author khernandez
 *
 */
@Repository
public interface EstatusDAO extends JpaRepository<Estatus, Long>, JpaSpecificationExecutor<Estatus> {
	

}
