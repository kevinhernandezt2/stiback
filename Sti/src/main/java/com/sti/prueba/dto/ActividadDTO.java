/**
 * 
 */
package com.sti.prueba.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author khernandez
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActividadDTO {
	
	private String nombreActividad;
	private long estatus;
	private String fechaHoraEjecucion;
	private long empleado;

}
