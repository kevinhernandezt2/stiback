/**
 * 
 */
package com.sti.prueba.dto;

import com.sti.prueba.models.entity.Empleado;
import com.sti.prueba.models.entity.Estatus;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author khernandez
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConsultarActividadesDTO {
	
	private long id;
	private String nombreActividad;
	private Estatus estatus;
	private String fechaHoraEjecucion;
	private int diasRetraso;
	private Empleado empleado;

}
