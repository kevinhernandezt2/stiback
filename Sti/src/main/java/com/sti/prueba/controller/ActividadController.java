/**
 * 
 */
package com.sti.prueba.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sti.prueba.dto.ActividadDTO;
import com.sti.prueba.dto.ConsultarActividadesDTO;
import com.sti.prueba.models.entity.Actividad;
import com.sti.prueba.service.IActividadService;

/**
 * @author khernandez
 *
 */
@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE })
@RestController
@RequestMapping("/actividades")
public class ActividadController {
	
	@Autowired
	private IActividadService actividadService;
	
	private static final String MENSAJE = "mensaje";
	private static final String RESPUESTA = "respuesta";
	private static final String ERROR = "error";
	
	@GetMapping("/consultar")
	public ResponseEntity<?> listarActividades(){
		Map<String, Object> response = new HashMap<>();
		try {
			List<ConsultarActividadesDTO> actividades = actividadService.listarActividades();
			
			response.put(MENSAJE, "Consulta generada con exito");
			response.put(RESPUESTA, actividades);
			response.put(ERROR, null);
			
		} catch (DataAccessException e) {
			response.put(MENSAJE, "Error al realizar consulta de actividades");
			response.put(RESPUESTA, null);
			response.put(ERROR, e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		}
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		
	}
	
	@PostMapping("/crear")
	public ResponseEntity<?> crearActividad(@RequestBody ActividadDTO actividadDto){
		
		Actividad actividadNueva = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			actividadNueva = actividadService.crearActividad(actividadDto);
		} catch (DataAccessException e) {
			response.put(MENSAJE, "Error al realizar el insert en la base de datos");
			response.put(RESPUESTA, null);
			response.put(ERROR, e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put(MENSAJE, "actividad ha sido creado con exito!");
		response.put(RESPUESTA, actividadNueva);
		response.put(ERROR, null);
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@PutMapping("/editar/{id}")
	public ResponseEntity<?> editarActividad(@RequestBody ActividadDTO actividadDto, @PathVariable Long id){
		
		Actividad actividadModificada = null;
		Map<String, Object> response = new HashMap<>();
		
		try {
			actividadModificada = actividadService.editarActividad(actividadDto, id);
		} catch (DataAccessException e) {
			response.put(MENSAJE, "Error al realizar el updated en la base de datos");
			response.put(RESPUESTA, null);
			response.put(ERROR, e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if (actividadModificada != null) {
			response.put(MENSAJE, "actividad ha sido modificada con exito!");
			response.put(RESPUESTA, actividadModificada);
			response.put(ERROR, null);
			
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
		}
		else {
			response.put(MENSAJE, "no se pudo editar, Actividad ID: ".concat(id.toString().concat(" no existe en la base de datos!")));
			response.put(RESPUESTA, null);
			response.put(ERROR, null);
			
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		}
		
	}
	
	@DeleteMapping("/eliminar/{id}")
	public ResponseEntity<?> eliminarActividad(@PathVariable Long id){
		Map<String, Object> response = new HashMap<>();
		try {
			actividadService.eliminarActividad(id);
			
		} catch (DataAccessException e) {
			response.put(MENSAJE, "Error al eliminar la actividad de la base de datos");
			response.put(RESPUESTA, null);
			response.put(ERROR, e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put(MENSAJE, "se elimino la actividad con exito!");
		response.put(RESPUESTA, null);
		response.put(ERROR, null);
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		
	}
}
