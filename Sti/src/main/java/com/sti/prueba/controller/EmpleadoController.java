/**
 * 
 */
package com.sti.prueba.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sti.prueba.models.entity.Empleado;
import com.sti.prueba.service.IEmpleadoService;

/**
 * @author khernandez
 *
 */
@CrossOrigin(origins = "*", methods = { RequestMethod.GET })
@RestController
@RequestMapping("/empleados")
public class EmpleadoController {
	
	@Autowired
	private IEmpleadoService empleadoService;
	
	private static final String MENSAJE = "mensaje";
	private static final String RESPUESTA = "respuesta";
	private static final String ERROR = "error";
	
	@GetMapping("/consultar")
	public ResponseEntity<?> listarEmpleados(){
		Map<String, Object> response = new HashMap<>();
		try {
			List<Empleado> empleados = empleadoService.listarEmpleados();
			
			response.put(MENSAJE, "Consulta generada con exito");
			response.put(RESPUESTA, empleados);
			response.put(ERROR, null);
			
		} catch (DataAccessException e) {
			response.put(MENSAJE, "Error al realizar consulta de empleados");
			response.put(RESPUESTA, null);
			response.put(ERROR, e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		}
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
		
	}

}
