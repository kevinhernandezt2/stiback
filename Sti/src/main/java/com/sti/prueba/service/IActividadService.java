/**
 * 
 */
package com.sti.prueba.service;

import java.util.List;

import com.sti.prueba.dto.ActividadDTO;
import com.sti.prueba.dto.ConsultarActividadesDTO;
import com.sti.prueba.models.entity.Actividad;

/**
 * @author khernandez
 *
 */
public interface IActividadService {
	
	public List<ConsultarActividadesDTO> listarActividades();
	public Actividad crearActividad(ActividadDTO actividad);
	public Actividad editarActividad(ActividadDTO actividad, Long id);
	public void eliminarActividad(Long id);

}
