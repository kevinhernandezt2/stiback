/**
 * 
 */
package com.sti.prueba.service;

import java.util.List;

import com.sti.prueba.models.entity.Empleado;

/**
 * @author khernandez
 *
 */
public interface IEmpleadoService {
	
	public List<Empleado> listarEmpleados();

}
