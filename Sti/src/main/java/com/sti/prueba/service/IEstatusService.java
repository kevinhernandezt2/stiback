/**
 * 
 */
package com.sti.prueba.service;

import java.util.List;

import com.sti.prueba.models.entity.Estatus;

/**
 * @author khernandez
 *
 */
public interface IEstatusService {
	
	public List<Estatus> listarEstatus();

}
