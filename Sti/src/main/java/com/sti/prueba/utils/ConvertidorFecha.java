/**
 * 
 */
package com.sti.prueba.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author khernandez
 *
 */
public class ConvertidorFecha {
	
	public static Timestamp formateoFechaStringTimestamp(String fecha) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    Date date = null;
		try {
			date = dateFormat.parse(fecha);
		} catch (ParseException e) {
			e.printStackTrace(System.err);
		}
		Timestamp timeStampDate = new Timestamp(date.getTime());
		return timeStampDate;
	}

}
